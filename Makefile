##
##  rhrdtime
##
##  The Radio Helsinki Rivendell Time Websocket Server
##
##
##  Copyright (C) 2015 Christian Pointner <equinox@helsinki.at>
##
##  This file is part of rhrdtime.
##
##  rhrdtime is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  rhrdtime is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with rhrdtime. If not, see <http://www.gnu.org/licenses/>.
##


curdir:= $(shell pwd)
GOCMD := GOPATH=$(curdir) go

EXECUTEABLE := rhrdtime

LIBS := "github.com/gorilla/websocket" \
        "github.com/coreos/go-systemd/activation"


.PHONY: getlibs updatelibs vet format build clean distclean
all: build


getlibs:
	@$(foreach lib,$(LIBS), echo "fetching lib: $(lib)"; $(GOCMD) get $(lib);)

updatelibs:
	@$(foreach lib,$(LIBS), echo "updating lib: $(lib)"; $(GOCMD) get -u $(lib);)

vet:
	@echo "vetting: $(EXECUTEABLE)"
	@$(GOCMD) vet $(EXECUTEABLE)

format:
	@echo "formating: $(EXECUTEABLE)"
	@$(GOCMD) fmt $(EXECUTEABLE)

build: getlibs
	@echo "installing: $(EXECUTEABLE)"
	@$(GOCMD) install $(EXECUTEABLE)


clean:
	rm -rf pkg/*/$(EXECUTEABLE)
	rm -rf bin

distclean: clean
	@$(foreach dir,$(shell ls src/),$(if $(subst $(EXECUTEABLE),,$(dir)),$(shell rm -rf src/$(dir))))
	rm -rf pkg
